import { Component, OnInit } from '@angular/core';
import { BatchService } from '../batch.service';
import { Batch } from './batch';
import { clone } from 'lodash';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-batch',
  templateUrl: './batch.component.html',
  styleUrls: ['./batch.component.css'],
  providers: [BatchService]
})
export class BatchComponent implements OnInit {
  batches: Batch[];
  batchForm: boolean = false;
  editBatchForm: boolean = false;
  isNewForm: boolean;
  newBatch: any = {};
  editedBatch: any = {};

  constructor(private _batchService: BatchService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getBatchs();
  }
 
  getBatchs() {
    this._batchService.getBatchsFromData().subscribe(data => {
      if (data) {
        this.batches = data;
      }
    });
  }

  showEditBatchForm(content, batch: Batch) {
    if(!batch) {
      this.batchForm = false;
      return;
    }
    this.editBatchForm = true;
    this.editedBatch = clone(batch);
    this.open(content);
  }

  showAddBatchForm(content) {
    // resets form if edited batch
    if(this.batches.length) {
      this.newBatch = {};
    }
    this.batchForm = true;
    this.isNewForm = true;
    this.open(content);
  }

  saveBatch(batch: Batch) {
    if(this.isNewForm) {
      // add a new batch
      this._batchService.addBatch(batch).subscribe();
      this.getBatchs();
    }
    this.batchForm = false;
  }

  removeBatch(batch: Batch) {
    this._batchService.deleteBatch(batch).subscribe();
    this.getBatchs();
  }

  updateBatch() {
    this._batchService.updateBatch(this.editedBatch).subscribe();
    this.editBatchForm = false;
    this.editedBatch = {};
    this.getBatchs();
  }

  cancelNewBatch() {
    this.newBatch = {};
    this.batchForm = false;
  }

  cancelEdits() {
    this.editedBatch = {};
    this.editBatchForm = false;
  }

  open(content) {
    this.modalService.open(content)/*.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    })*/;
  }
}
