export interface Batch {
  id: number;
  name: string;
  description: string;
  accountingPeriod: string;
  completationStatus: string;
  batchStatus: string;
}