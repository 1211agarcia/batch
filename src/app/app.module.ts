import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BatchComponent } from './batch/batch.component';
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
// Routes
import { _ROUTING } from './app.routes';

// Services
import { BatchService } from './batch.service';

@NgModule({
  declarations: [
    AppComponent,
    BatchComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    _ROUTING,
    HttpModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
