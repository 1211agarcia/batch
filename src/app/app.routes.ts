/*
 * @author: Armando Garcia <1211agarcia@gmail.com>
 * Created on 2017-10-26 16:30:16
 */
import { Routes, RouterModule } from '@angular/router';
import { BatchComponent } from './batch/batch.component';
const routes: Routes = [
    {
        path: 'batch',
        component: BatchComponent
    }];

export const _ROUTING = RouterModule.forRoot(routes);
