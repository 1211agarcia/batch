import { Injectable } from '@angular/core';
import { Batch } from './batch/batch';
import { findIndex } from 'lodash';

import { Headers, Http, RequestOptions  } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class BatchService {
  options;
  domain = 'http://localhost:3000/';;

  constructor(
    private http: Http
  ) { }

   // Function to create headers, add token, to be used in HTTP requests
   createAuthenticationHeaders() {
    // Headers configuration options
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json', // Format set to JSON
      })
    });
  } 
  getBatchsFromData(){
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + 'api/batch', this.options)
      .map(res => res.json());
  }

  addBatch(batch: Batch) {
    this.createAuthenticationHeaders();
    return this.http.post(this.domain + 'api/batch', batch, this.options)
      .map(res => res.json());
  }

  updateBatch(batch: Batch) {
    this.createAuthenticationHeaders();
    return this.http.put(this.domain + 'api/batch/'+batch.id,batch, this.options)
      .map(res => res.json());
  }

  deleteBatch(batch: Batch) {
    this.createAuthenticationHeaders();
    return this.http.delete(this.domain + 'api/batch/'+batch.id, this.options)
      .map(res => res.json());
  }

}