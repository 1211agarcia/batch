# Bath Frontend Test

Bath Frontend Test es un desarrollo del frontend, donde  mediante solicitudes y peticiones REST, se puede procesar información del registro de BATCHES.

# Requerimientos
A continuación, para ejecutar localmente debe contar con los siguientes requisitos.
  - Git
  - NodeJs [install](https://nodejs.org/en/)
  - Angular CLI [install](https://github.com/angular/angular-cli) version 1.4.9.
  - Json-serve [install](https://github.com/typicode/json-server)

# Instalación

  - Abre el Terminal - Ubica en el directorio donde quieres descarga el proyecto
  - Ejecutar: 
```
        $ git clone https://1211agarcia@bitbucket.org/1211agarcia/batch.git 
```
```
        $ cd batch // Ingresa al directorio batch
```
```
        $ npm install // esto descarga dependencias del proyecto
```
```
        $ ng serve -o // Se ejecuta el servidor local 
```
  
# Configuración de la BD
  - Para ejecura el servidor backend Abrel el Terminal - Ubica en el directorio donde quieres descarga el proyecto
  - Ejecutar: 
```
        $ cd backend
```
```
        $ json-server db.json --routes routes.json 
```

###### Nota: Para realizar el presente desarrollo se realizó bajo suposiciones dado el tiempo y la situación de la prueba.
- No se realizaron validaciones de campos.
- La documentación del sistema es mas funcional que a nivel de código. La documentación necesaria esta por el repositorio mediante los README.md.